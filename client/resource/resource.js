    Template.resource.onCreated(function(){
        Session.set('tagfilter', false );
        Session.set('sort', "date+");
        Session.set('tagsort', new Array);
    });

    Template.resource.helpers({
        tasks: function () {
            // if (Session.get("hideCompleted")) {
            //     // If hide completed is checked, filter tasks
            //     return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
            // } else {
            //     // Otherwise, return all of the tasks
                // return Tasks.find({}, { sort: { createdAt: -1 } });
            // }
            if(Session.get('tagfilter')){

                switch(Session.get('sort')) {
                    case "abc+":
                            return Tasks.find({ tags: { $all: Session.get('tagsort') } }, { sort: { text: 1 } });
                        break;
                    case "abc-":
                            return Tasks.find({ tags: { $all: Session.get('tagsort') } }, { sort: { text: -1 } });
                        break;
                    case "date+":
                            return Tasks.find({ tags: { $all: Session.get('tagsort') } }, { sort: { createdAt: -1 } });
                        break;
                    case "date-":
                            return Tasks.find({ tags: { $all: Session.get('tagsort') } }, { sort: { createdAt: 1 } });
                        break;
                    default:
                            return Tasks.find({ tags: { $all: Session.get('tagsort') } }, { sort: { createdAt: -1 } });
                }

            }
            else {

                switch(Session.get('sort')) {
                    case "abc+":
                            return Tasks.find({}, { sort: { text: 1 } });
                        break;
                    case "abc-":
                            return Tasks.find({}, { sort: { text: -1 } });
                        break;
                    case "date+":
                            return Tasks.find({}, { sort: { createdAt: -1 } });
                        break;
                    case "date-":
                            return Tasks.find({}, { sort: { createdAt: 1 } });
                        break;
                    default:
                            return Tasks.find({}, { sort: { createdAt: -1 } });
                }
            }

        },

        tags: function () {
            return Tags.find({}, {sort: {count: -1},limit:12});
        }
    });

  Template.resource.events({
      "change #sort": function () {
            var newValue = $(event.target).val();
            Session.set('sort', newValue);
      }
  });
