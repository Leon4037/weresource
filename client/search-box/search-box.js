Template.searchbox.events({
  "keyup #searchbox": _.throttle(function(e) {
    var text = $(e.target).val().trim();
    PackageSearch.search(text);
  }, 200),




  tasks: function () {
      if (Session.get("hideCompleted")) {
          // If hide completed is checked, filter tasks
          return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
      } else {
          // Otherwise, return all of the tasks
          return Tasks.find({}, { sort: { createdAt: -1 } });
      }
  },
  tags: function () {


if (Session.get("hideCompleted")) {
// If hide completed is checked, filter tasks
return Tags.find({checked: {$ne: true}}, {sort: {createdAt: -1}});
}
else {
// Otherwise, return all of the tasks
return Tags.find({}, {sort: {createdAt: -1}});
}
},
});
