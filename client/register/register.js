/* global firmenkunde */
Template.register.onCreated(function(){
    Session.set('firmenkunde', false);
    Session.set('neuefirma', false);
});

Template.register.events({
    'submit #register': function(event){
        event.preventDefault();
        var email = $('[name=email]').val();
        var password = $('[name=password]').val();
        var username = $('[name=username]').val();
        var vorname = $('[name=vorname]').val();
        var nachname = $('[name=nachname]').val();
        if (Session.get('firmenkunde')){
            var firmenname = $('[id=firmen]').val();
            var firmenid = Firmen.findOne({ name: firmenname})._id;
            var profil = {vorname: vorname, nachname: nachname, firmenkunde: true, firmenname: firmenname, firmenid: firmenid};
        }
        else {
            var profil = {vorname: vorname, nachname: nachname, firmenkunde: false};
        }
        Accounts.createUser({
            email: email,
            password: password,
            username: username,
            profile: profil,
        });
        console.log(profil);
        Router.go('Waste');
    },

      "change #firma": function () {
            Session.set('firmenkunde', true);
      },

      "change #privat": function () {
            Session.set('firmenkunde', false);
      },

      'click .neuefirma': function() {
          var neuefirma = !Session.get('neuefirma');
          Session.set('neuefirma', neuefirma);
      },

      "click #submitter": function(event) {
          event.preventDefault();
          var firmenname = $('[name=firmenname]').val();
          var strasse = $('[name=strasse]').val();
          var nummer = $('[name=nummer]').val();
          var ort = $('[name=ort]').val();
          var plz = $('[name=plz]').val();

          Meteor.call("addFirma", firmenname, strasse, nummer, plz, ort);

          $(' [name=firmenname]').val("");
          $(' [name=strasse]').val("");
          $(' [name=nummer]').val("");
          $(' [name=plz]').val("");
          $(' [name=ort]').val("");
      }
});

Template.register.helpers({
    firmenkunde: function(){
        return Session.get('firmenkunde');
    },

    neuefirma: function(){
        return Session.get('neuefirma');
    },

    firmen: function(){
        return Firmen.find();
    }
});
