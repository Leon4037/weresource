  Template.aktivtag.events({
    "click .delete": function() {
      // Tags aus ausgewähltem Artikel auslesen
      var fetchedTags = Session.get('selectedfetchedTags');
      var mappedTags = Session.get('selectedmappedTags');

      // Tags werden aus der aktuellen Variable gelöscht
      var tagindex = mappedTags.indexOf(this._id);
      fetchedTags.splice(tagindex, 1);
      mappedTags.splice(tagindex, 1);

      // aktuelle Sessionvariable wir aktualisiert
      Session.set('selectedfetchedTags', fetchedTags);
      Session.set('selectedmappedTags', mappedTags);
    },
    "click .toggle-checked": function() {
      // Set the checked property to the opposite of its current value
      Meteor.call("setCheckedTags", this._id, !this.checked);
    }
  });