Template.login.events({
    'submit form': function(event){
        event.preventDefault();
        var email = $('[name=email]').val();
        var password = $('[name=password]').val();
        Meteor.loginWithPassword(email, password, function(err){
            if (err){
                sAlert.error(err.reason, {effect: 'genie', position: 'top', timeout: 'none', onRouteClose: true, stack: false, offset: '80px'});
            }
            else {
                Router.go('Home');  
            }
        });
    }
});
