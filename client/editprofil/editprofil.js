  Template.editprofil.helpers({
    clickedPW : function () {
        return Session.get('clickedPW');
    },
    editprofil: function () {
      return Meteor.user();
    }
  });

  Template.editprofil.events({
    "submit #edit-profil": function (event) {
      // Prevent default browser form submit
      event.preventDefault();

      var id = Meteor.userId();
      var username = event.target.username.value;
      var email = event.target.email.value;
      var vorname = event.target.vorname.value;
      var nachname = event.target.nachname.value;
      var firma = event.target.firma.value;
      Meteor.call("updateUser", id, username, email, vorname, nachname, firma);
      Router.go('profil');
    },

  "click .editpw": function (event) {
      // Prevent default browser form submit
      event.preventDefault();
      Session.set('clickedPW', true);
    },

  "submit .change-password": function (event) {
      // Prevent default browser form submit
      event.preventDefault();

        var id = Meteor.userId();
        var alt = event.target.oldpw.value;
        var neu = event.target.newpw.value;
        var neu2 = event.target.newpw2.value;
        if (neu == neu2){
            if (Accounts.changePassword(alt, neu)){
                console.log("Geändert")
            }
            else{
                console.log("nicht geändert, falsches pw");
            }
        }
        else {
            console.log("Neues Passwort stimmt nicht überein!!!");
        }

      Session.set('clickedPW', false);
    },
  });
