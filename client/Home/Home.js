/* global $ */
Template.Home.helpers({
    tasks: function () {
        if (Session.get("hideCompleted")) {
            // If hide completed is checked, filter tasks
            return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
        } else {
            // Otherwise, return all of the tasks
            return Tasks.find({}, { sort: { createdAt: -1 },limit:5 });
        }

    },
    tags: function () {
      return Tags.find({}, { sort: { count: -1 },limit:15 });
    },
        auswahl: function (){
        return Tags.find({checked: true});
    }

  });
