
Template.eigene.helpers({
	isOwner: function () {
		return this.owner === Meteor.userId();
	},
  reservations: function () {
      return Tasks.find({ owner: Meteor.userId() });
  }
});

Template.eigene.events({
	"click .toggle-checked": function () {
		// Set the checked property to the opposite of its current value
		Meteor.call("setChecked", this._id, !this.checked);
	},
	"click .showowner": function () {
		Session.set('ownerID', this.owner);
	},
	"click .delete": function () {
		Meteor.call("deleteTask", this._id);
		for (i = 0; i < this.fetchedTags.length; i++) {
			Meteor.call("decrementCount", this.fetchedTags[i]._id);
		}
	},

	"click .edit": function () {
		Session.set('selectedTaskID', this._id);
		Session.set('selectedfetchedTags', this.fetchedTags);
	    Session.set('selectedmappedTags', this.tags);
	},

	"click .toggle-private": function () {
		Meteor.call("setPrivate", this._id, !this.private);
	}
});
