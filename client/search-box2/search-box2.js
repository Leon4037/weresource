Template.searchbox2.events({
  "keyup #searchbox2": _.throttle(function(e) {
    var text = $(e.target).val().trim();
    PackageSearch2.search(text);
  }, 200),


      "submit .new-tag": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var tag = event.target.tag.value;

        // Insert a Tag into the collection
        Meteor.call("addTag", tag);

        // Clear form
        event.target.tag.value = "";

      },


  tasks: function () {
      if (Session.get("hideCompleted")) {
          // If hide completed is checked, filter tasks
          return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
      } else {
          // Otherwise, return all of the tasks
          return Tasks.find({}, { sort: { createdAt: -1 } });
      }
  },
  tags: function () {

PackageSearch2.search('');
if (Session.get("hideCompleted")) {
// If hide completed is checked, filter tasks
return Tags.find({checked: {$ne: true}}, {sort: {createdAt: -1}});
}
else {
// Otherwise, return all of the tasks
return Tags.find({}, {sort: {createdAt: -1}});
}
},
});
