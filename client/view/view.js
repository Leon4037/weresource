  Template.view.helpers({
    view: function () {
      var selectedTaskID = Session.get('selectedTaskID');
      var selectedTask = Tasks.findOne({ _id: selectedTaskID });
      return selectedTask;
    },
    
    tags: function () {
      var fetchedTags = Session.get('selectedfetchedTags');
      aktfetchedTags = new Array();
      for (var i = 0; i < fetchedTags.length; i++) {
        aktfetchedTags[i] = Tags.findOne(fetchedTags[i]._id);
      }
      // soll alle zum "selectedTask" gehörenden Tags returnen!
      return aktfetchedTags;
    },
    
    isOwner: function () {
        var selectedTaskID = Session.get('selectedTaskID');
        var selectedTask = Tasks.findOne({ _id: selectedTaskID });
        return selectedTask.owner === Meteor.userId();
	},
    
    reserverbool: function () {
        var selectedTaskID = Session.get('selectedTaskID');
        var selectedTask = Tasks.findOne({ _id: selectedTaskID });
        return selectedTask.reservedBy === Meteor.userId();
	},
    
    reserver: function () {
        var selectedTaskID = Session.get('selectedTaskID');
        var selectedTask = Tasks.findOne({ _id: selectedTaskID });
        var reserver = Meteor.users.findOne({ _id: selectedTask.reservedBy});
        var profil = reserver.profile;
        return profil.vorname + " " + profil.nachname;
	},
  });
  
    Template.view.events({
    'click .edit': function(event){
        Router.go('edit');
    },
    
    "click .toggle-private": function () {
        var selectedTaskID = Session.get('selectedTaskID');
        var artikel = Tasks.findOne(selectedTaskID);
        Meteor.call("setPrivate", selectedTaskID, !artikel.private);  
	},
    
    "click .toggle-reserved": function () {
        var selectedTaskID = Session.get('selectedTaskID');
        var artikel = Tasks.findOne(selectedTaskID);
        Meteor.call("setReservedBy", selectedTaskID, Meteor.userId());
        Meteor.call("setPrivate", selectedTaskID, !artikel.private);  
        Meteor.call("setReserved", selectedTaskID, !artikel.reserved);
	}
    
    });