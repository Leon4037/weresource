/* global aktfetchedTags */
/* global Meteor */
/* global Template */
/* global Tags */
/* global Session */
/* global Tasks */
/* global selectedTask */
/* global selectedTaskID */

    Template.edit.onCreated(function(){
      Meteor.call("resetCheckedTags", false);
      var selectedTaskID = Session.get('selectedTaskID');
      var selectedTask = Tasks.findOne({ _id: selectedTaskID });
      var tags = selectedTask.tags;
      for (var i = 0; i < tags.length; i++) {
          Meteor.call("setCheckedTags", tags[i], true)
      }
      Session.set('fotokey', "no photos");
    });

  Template.edit.helpers({
    edit: function () {
      var selectedTaskID = Session.get('selectedTaskID');
      var selectedTask = Tasks.findOne({ _id: selectedTaskID });
      return selectedTask;
    },

    auswahl: function (){
        return Tags.find({checked: true});
    },
    
    foto: function (){
        if (Session.get('fotokey') == "no photos") {
            var selectedTaskID = Session.get('selectedTaskID');
            var selectedTask = Tasks.findOne({ _id: selectedTaskID });
            return selectedTask.Phkey;
        }
        else {
            return Session.get('fotokey').url;
        }
    }

  });

  Template.edit.events({
    "submit .edit-task": function (event) {
      // Prevent default browser form submit
      event.preventDefault();
      console.log("hat funktioniert");
      // Get value from form element
      var text = event.target.text.value;
      var masse = event.target.masse.value;
      var info = event.target.info.value;

      var selectedTaskID = Session.get('selectedTaskID');
      var selectedTask = Tasks.findOne({ _id: selectedTaskID });


      var newfetchedTags = Tags.find({checked: true}).fetch();
      var newmappedTags = newfetchedTags.map( function(tag) { return tag._id; });
      var mappedTags = selectedTask.tags;

      for (var i = 0; i < newmappedTags.length; i++){
        if (mappedTags.indexOf(newmappedTags[i]) == -1){
          Meteor.call("incrementCount", newmappedTags[i]);
        }
      }

      for (var i = 0; i < mappedTags.length; i++){
        if (newmappedTags.indexOf(mappedTags[i]) == -1){
          Meteor.call("decrementCount", mappedTags[i]);
        }
      }
      
      if(Session.get('fotokey')!= "no photos"){
          var phkey = Session.get("fotokey").url;
      }
      else {
          var phkey = "Logo_foto.png";
      }
      
      Meteor.call("updatePhoto",selectedTask, phkey);

      // Insert a task into the collection
      Meteor.call("updateTask",selectedTask, text, masse, info, newfetchedTags, newmappedTags);

      // Clear form
      event.target.text.value = "";
      event.target.masse.value = "";
      event.target.info.value = "";
      Meteor.call("resetCheckedTags", false);

      Router.go('resource');
    },
    
    'change #userimage' : function(event){
      event.preventDefault();
      var files = [];
      var file = $('#userimage')[0].files[0];
      files.push(file);
      console.log(files);
      Cloudinary._upload_file(files[0], {}, function (err, res) {
        console.log("Upload Error: " + err);
        console.log("Upload Result: " + res);
        Session.set("fotokey", res);
        sAlert.success('Sie haben erfolgreich ein Bild hinzugefügt.', {effect: 'genie', position: 'top', timeout: 'none', onRouteClose: false, stack: false, offset: '80px'});
      });
    },
  });
