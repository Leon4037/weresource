/* global $ */

if (Meteor.isClient) {
    var thephoto;
  //Cloudinary connection setup
  $.cloudinary.config({
    cloud_name: "weresource"
  });
  Session.setDefault("fotokey", "no photos");

    Template.Waste.onCreated(function(){
        Meteor.call("resetCheckedTags", false);
        Session.set("fotokey", "no photos");
    });

  Template.Waste.helpers({
    tasks: function () {

      /*if (Session.get("hideCompleted")) {
        // If hide completed is checked, filter tasks
        return Tasks.find({checked: {$ne: true}}, {sort: {createdAt: -1}});
      } else {
        // Otherwise, return all of the tasks
        return Tasks.find({}, {sort: {createdAt: 1}});
      }
      return Tasks.find({}, {sort: {createdAt: 1}});
      },
        hideCompleted: function () {
          return Session.get("hideCompleted");
        },
      incompleteCount: function () {
          return Tasks.find({checked: {$ne: true}}).count();*/
    },

    tags: function () {
      return Tags.find({}, { sort: { count: -1 },limit:15 });
    },

    auswahl: function (){
        return Tags.find({checked: true});
    },

    foto: function (){
        if (Session.get('fotokey') == "no photos") {
            return "uploadalternative.jpg";
        }
        else {
            return Session.get('fotokey').url;
        }
    },
    
    firmenkunde: function() {
        return Meteor.user().profile.firmenkunde;
    }

  });

  Template.Waste.events({
    "submit .new-task": function (event) {
      // Prevent default browser form submit
      event.preventDefault();

      // Get value from form element
      var text = event.target.text.value;
      var masse = event.target.masse.value;
      var info = event.target.info.value;
      var tags = Tags.find({ checked: { $ne: false }});
      var phkey = Session.get("fotokey");    
      if (phkey != "no photos"){
             phkey = Session.get("fotokey").url;
      }
      else {
          phkey = "Logo_foto.png"
      }
      // erstellt Array aus IDs der angekreuzten Tags
      var mappedTags = tags.map( function(tag) { return tag._id; });
      for (var i = 0; i < mappedTags.length; i++) {
        Meteor.call("incrementCount", mappedTags[i]);
      }
      tags = Tags.find({ checked: { $ne: false }});
      // erstellt Array aus Objekten der angekreuzten Tags
      var fetchedTags = tags.fetch();
      // for (i = 0; i < fetchedTags.length; i++) {
      //   Meteor.call("incrementCount", fetchedTags[i].name)
      // }

      // Insert a task into the collection
      Meteor.call("addTask", text, masse, mappedTags, fetchedTags, info, phkey);
      // Clear form
      event.target.text.value = "";
      event.target.masse.value = "";
      event.target.info.value = "";
      Meteor.call("resetCheckedTags", false);
      Session.set("fotokey", "no photos");
      sAlert.success('Sie haben erfolgreich einen Artikel erstellt.', {effect: 'genie', position: 'top', timeout: 'none', onRouteClose: false, stack: false, offset: '80px'});
    },

    "change .hide-completed input": function (event) {
      Session.set("hideCompleted", event.target.checked);
	   },

    "submit .new-tag": function (event) {
      // Prevent default browser form submit
      event.preventDefault();

      // Get value from form element
      var tag = event.target.tag.value;

      // Insert a Tag into the collection
      Meteor.call("addTag", tag);

      // Clear form
      event.target.tag.value = "";

    },

    'change #userimage' : function(event){
      event.preventDefault();

      var files = [];
      var file = $('#userimage')[0].files[0];
      files.push(file);
      console.log(files);
      Cloudinary._upload_file(files[0], {}, function (err, res) {
        console.log("Upload Error: " + err);
        console.log("Upload Result: " + res);
        Session.set("fotokey", res);
        sAlert.success('Sie haben erfolgreich ein Bild hinzugefügt.', {effect: 'genie', position: 'top', timeout: 'none', onRouteClose: true, stack: false, offset: '80px'});
      });
    },
  });
}
