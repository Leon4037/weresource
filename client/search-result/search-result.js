Template.searchresult.helpers({
getTasks: function() {

    return PackageSearch.getData({
      transform: function(matchText, regExp) {
        return ("" + matchText).replace(regExp, "<b>$&</b>")
      },
      sort: {isoScore: -1},
      limit:5
    });

  },




  isLoading: function() {
    return PackageSearch.getStatus().loading;
  },
  tasks: function () {
      if (Session.get("hideCompleted")) {
          // If hide completed is checked, filter tasks
          return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
      } else {
          // Otherwise, return all of the tasks
          return Tasks.find({}, { sort: { createdAt: -1 }, limit:5 });
      }
  },
  tags: function () {

if (Session.get("hideCompleted")) {
// If hide completed is checked, filter tasks
return Tags.find({checked: {$ne: true}}, {sort: {createdAt: -1},limit:5});
}
else {
// Otherwise, return all of the tasks
return Tags.find({}, {sort: {createdAt: -1},limit:5});
}
},
});
Template.searchresult.events({


	"click .edit": function () {
		Session.set('selectedTaskID', this._id);
		Session.set('selectedfetchedTags', this.fetchedTags);
	    Session.set('selectedmappedTags', this.tags);
	},


});
Template.searchresult.rendered = function() {
  PackageSearch.search('');
};
