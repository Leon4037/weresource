Template.searchresult2.helpers({
getTags: function() {

    return PackageSearch2.getData({
      transform: function(matchText, regExp) {
        return ("" + matchText).replace(regExp, "$&")
      },
      sort: {isoScore: -1}
    });
  },




  isLoading: function() {
    return PackageSearch2.getStatus().loading;
  },
  tasks: function () {
      if (Session.get("hideCompleted")) {
          // If hide completed is checked, filter tasks
          return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
      } else {
          // Otherwise, return all of the tasks
          return Tasks.find({}, { sort: { createdAt: -1 } });
      }
  },
  tags: function () {

if (Session.get("hideCompleted")) {
// If hide completed is checked, filter tasks
return Tags.find({checked: {$ne: true}}, {sort: {createdAt: -1}});
}
else {
// Otherwise, return all of the tasks
return Tags.find({}, {sort: {createdAt: -1}});
}
},
});

Template.searchresult2.rendered = function() {
  PackageSearch2.search('');
};

  Template.searchresult2.events({
    "click .delete": function () {
      Meteor.call("deleteTag", this._id);
    },
    "click .toggle-checked": function () {
      // Set the checked property to the opposite of its current value
      console.log(this._id);
      console.log(this.checked);
      Meteor.call("setCheckedTags", this._id, !this.checked);
    }
  });
