  Template.profil.helpers({
    profil: function () {
      return Meteor.user();
    },
    
    firma: function() {
        return Firmen.findOne(Meteor.user().profile.firmenid);
    },
    firmenkunde: function() {
        return Meteor.user().profile.firmenkunde;
    }
  });
  
  Template.profil.events({
    'click .edit': function(event){
        Session.set('clickedPW', false);
        Router.go('editprofil');
    }
});