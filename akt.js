/* global Session */
/* global Template */
/* global Meteor */
/* global Cloudinary */
/* global Photos */
/* global Mongo */
/* global MeteorCamera */
/* global Accounts */
Tasks = new Mongo.Collection("tasks");
Tags = new Mongo.Collection("tags");
Photos = new Mongo.Collection("photos");
Firmen = new Mongo.Collection("firmen");
if (Meteor.isServer) {
   Meteor.AppCache.config({onlineOnly: ['/online/']});
    // This code only runs on the server
    // Only publish tasks that are public or belong to the current user

    Meteor.publish("tasks", function () {
        return Tasks.find({
            // $or: [ {$or: [ { private: { $ne: true } }, { owner: this.userId }]}, {$or: [{ reservedBy: this.userId }, { reserved: { $ne: true} }]} ]
            $or: [ { private: { $ne: true } }, { owner: this.userId }, { reservedBy: this.userId }]
        });
    });

    Meteor.publish("tags", function () {
        return Tags.find({

        });
    });

    Meteor.publish("firmen", function () {
        return Firmen.find({

        });
    });

    Meteor.publish("users", function () {
        return Meteor.users.find();
    });

    Cloudinary.config({
        cloud_name: 'weresource',
        api_key: '643269449732476',
        api_secret: 'FdGk9Jvk9_5npEFUsqF_qdKfDjs'
    });

  SearchSource.defineSource('tasks', function(searchText, options) {
  var options = {sort: {isoScore: -1}, limit: 17};

  if(searchText) {
    var regExp = buildRegExp(searchText);
    var selector ={text: regExp,info: regExp};

    return Tasks.find(selector, options).fetch()[0];
  }

  else {

    return Tasks.find({}, options).fetch();

  }
});

SearchSource.defineSource('tags', function(searchText, options) {
  var options = {sort: {isoScore: -1}, limit:  17};

  if(searchText) {
    var regExp = buildRegExp(searchText);
    var selector ={text: regExp,info: regExp, name:regExp};

    return Tags.find(selector, options).fetch()[0];
  }

  else {

    return Tags.find({}, options).fetch();

  }
});

function buildRegExp(searchText) {
  var words = searchText.trim().split(/[ \-\:]+/);
  var exps = _.map(words, function(word) {
    return "(?=.*" + word + ")";
  });
  var fullExp = exps.join('') + ".+";
  return new RegExp(fullExp, "i");
}

}//SERVER
Photos.allow({

    insert: function () {
        return true;
    },
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    }
});


if (Meteor.isClient) {

  Meteor.startup(function () {

      sAlert.config({
          effect: '',
          position: 'top-right',
          timeout: 5000,
          html: false,
          onRouteClose: true,
          stack: true,
          // or you can pass an object:
          // stack: {
          //     spacing: 10 // in px
          //     limit: 3 // when fourth alert appears all previous ones are cleared
          // }
          offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
          beep: false,
          // examples:
          // beep: '/beep.mp3'  // or you can pass an object:
          // beep: {
          //     info: '/beep-info.mp3',
          //     error: '/beep-error.mp3',
          //     success: '/beep-success.mp3',
          //     warning: '/beep-warning.mp3'
          // }
          onClose: _.noop //
          // examples:
          // onClose: function() {
          //     /* Code here will be executed once the alert closes. */
          // }
      });

  });
  // subscribed = false
  //   Tracker.autorun(function () {
  //     if (!subscribed) {
  //       Meteor.subscribe('people');
  //       subscribed = true;
  //     }
  //   });

  subscribed = false
 Tracker.autorun(function() {

 if (!Meteor.userId() && !subscribed) {
   Meteor.subscribe("tasks");
   Meteor.subscribe("tags");
   Meteor.subscribe ("firmen");
   Meteor.subscribe("users");
   subscribed = true;
 }
  });
   // This code only runs on the client
   Meteor.subscribe("tasks");
   Meteor.subscribe("tags");
   Meteor.subscribe("firmen");
   //CLIENT

   var options = {
     keepHistory: 0,
     localSearch: true
   };
   var fields = ['text', 'info', 'name', 'tags'];

   PackageSearch = new SearchSource('tasks', fields, options);
   PackageSearch2 = new SearchSource('tags', fields, options);








  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });

    // This code only runs on the client
    Meteor.subscribe("tasks");
    Meteor.subscribe("tags");
    Meteor.subscribe("users");
    //CLIENT




    Template.standard.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
  Router.go('Home');
    }
    });

    Template.standard.helpers({
    Username: function(event){
        return Meteor.user().username;
    }
    });
}
