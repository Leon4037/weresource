/* global Router */
// Router.configure({
// 	layoutTemplate:'standard'
// });
// Router.map(function(){
// 	this.route('Waste',{path:'/Waste'});
// 	this.route('resource',{path:'/Resource'});
// 	this.route('edit',{path:'/edit'});
// 	this.route('register',{path:'/register'});
//     this.route('login',{path:'/login'});
//     this.route('profil',{path:'/profil'});
//     this.route('editprofil',{path:'/editprofil'});
//     this.route('view',{path:'/view'});
// });
Router.route('/',function(){
    this.layout("standard");
    this.render('Home');
});
Router.route('/Home',function(){
    this.layout("standard");
    this.render('Home');
});
Router.route('/eigene',function(){
    this.layout("standard");
    this.render('eigene');
});
Router.route('/Agb',function(){
    this.layout("standard");
    this.render('Agb');
});
Router.route('/FAQ',function(){
    this.layout("standard");
    this.render('FAQ');
});
Router.route('/Impressum',function(){
    this.layout("standard");
    this.render('Impressum');
});
Router.route('/ueber',function(){
    this.layout("standard");
    this.render('ueber');
});
Router.route('/Waste',function(){
    this.layout("standard");
    this.render('Waste');
});
Router.route('/resource',function(){
    this.layout("standard");
    this.render('resource');
});
Router.route('/edit',function(){
    this.layout("standard");
    this.render('edit');
});
Router.route('/register',function(){
    this.layout("standard");
    this.render('register');
});
Router.route('/login',function(){
    this.layout("standard");
    this.render('login');
});
Router.route('/profil',function(){
    this.layout("standard");
    this.render('profil');
});
Router.route('/view',function(){
    this.layout("standard");
    this.render('view');
});
Router.route('/editprofil',function(){
    this.layout("standard");
    this.render('editprofil');
});
Router.route('/myreservations',function(){
    this.layout("standard");
    this.render('myreservations');
});
Router.route('/ownerprofil',function(){
    this.layout("standard");
    this.render('ownerprofil');
});
