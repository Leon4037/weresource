/* global i */
/* global Meteor */
/* global Tasks */
/* global Tags */
Meteor.methods({
  addTask: function (text, masse, mappedTags, fetchedTags, info, phkey) {
    // Make sure the user is logged in before inserting a task
    if (!Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

    var tagstring = new Array();
    for (i = 0; i < fetchedTags.length; i++) {
      tagstring.push(fetchedTags[i].name)
    }

    Tasks.insert({
      text: text,
      createdAt: new Date(),
      datum: new Date().toDateString(),
      owner: Meteor.userId(),
      username: Meteor.user().username,
      masse: masse,
      tags: mappedTags,
      fetchedTags: fetchedTags,
      info: info,
      tagstring: tagstring.join(" || "),
      Phkey: phkey
    });
  },


   updateTask: function (selectedTask, text, masse, info, fetchedTags, mappedTags) {
    var tagstring = new Array();
    for (i = 0; i < fetchedTags.length; i++) {
      tagstring.push(fetchedTags[i].name)
    }

    Tasks.update(selectedTask, {$set:
    {
      text: text,
      masse: masse,
      info: info,
      fetchedTags: fetchedTags,
      tags: mappedTags,
      tagstring: tagstring.join(" || ")
    }});
  },
  
  updatePhoto: function (selectedTask, phkey){
   Tasks.update(selectedTask, {$set:
    {
        Phkey: phkey
    }});  
  },

  deleteTask: function (taskId) {
	   var task = Tasks.findOne(taskId);
    if (task.private && task.owner !== Meteor.userId()) {
      // If the task is private, make sure only the owner can delete it
      throw new Meteor.Error("not-authorized");
    }
    Tasks.remove(taskId);
  },

  addTag: function (tag) {
    // Make sure the user is logged in before inserting a task
    if (!Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

    if (tag != "") {
      if (!Tags.findOne({ name: tag })) {
        Tags.insert({
          name: tag,
          count: 0,
          checked: true
        });
      }
    }
  },

  deleteTag: function (tagId) {
    Tags.remove(tagId);
  },

  incrementCount: function (tagid) {
    var selectedTag = Tags.findOne({ _id: tagid });
    Tags.update(selectedTag, { $inc: { count: 1 } });
  },

  decrementCount: function (tagid) {
    var selectedTag = Tags.findOne({ _id: tagid });
    Tags.update(selectedTag, { $inc: { count: -1 } });
  },

  setChecked: function (taskId, setChecked) {
    var task = Tasks.findOne(taskId);
    if (task.private && task.owner !== Meteor.userId()) {
      // If the task is private, make sure only the owner can check it off
      throw new Meteor.Error("not-authorized");
    }
    Tasks.update(taskId, { $set: { checked: setChecked } });
  },

  setCheckedTags: function (tagId, setChecked) {
    Tags.update(tagId, { $set: { checked: setChecked } });
  },
  
  resetCheckedTags: function (wert) {
      Tags.update({}, { $set: { checked: wert } }, { multi: true } );
  },

  setPrivate: function (taskId, setToPrivate) {
    var task = Tasks.findOne(taskId);
    // Make sure only the task owner can make a task private
    // if (task.owner !== Meteor.userId()) {
    //   throw new Meteor.Error("not-authorized");
    // }
    Tasks.update(taskId, { $set: { private: setToPrivate } });
  },
  
  setReserved: function (taskId, setToReserved) {
    var task = Tasks.findOne(taskId);
    Tasks.update(taskId, { $set: { reserved: setToReserved} });
  },

  updateUser: function (id, username, email, vorname, nachname, firma) {
      var profil = {vorname: vorname, nachname: nachname, firma: firma};
      Meteor.users.update({_id: id}, { $set: {
                                            username: username,
                                            email: email,
                                            profile: profil
                                              }
                                            });
  },
  
  setReservedBy: function (artikelId, UserId){
      var artikel = Tasks.findOne(artikelId);
      if (artikel.reserved){
        Tasks.update(artikelId, { $set: { reservedBy: null } });
      }
      else {
        Tasks.update(artikelId, { $set: { reservedBy: UserId } });
      }
  },
  
  userNames: function (userId){
     var user = Meteor.users.findOne({_id: userId});
     var profile = user.profile;
     console.log(profile.vorname + profile.nachname)
     return profile.vorname + profile.nachname;
  },
  
  addFirma: function (name, strasse, nummer, plz, ort) {
    Firmen.insert({
        name: name,
        strasse: strasse,
        nummer: nummer,
        plz: plz,
        ort: ort
    });
  },
});
